#### Certificado SSL
- Ir até Applications -> Routes
- Escolher a build que desejar
- Clicar em Actions do lado superior direito e Edit
- Habilitar Secure route
- Clicar em Save

Só com estas etapas, o certificado do OKD será inserido na rota.

#### Criando um certificado auto assinado
```shell
openssl req -x509 -nodes -keyout key.pem -out cert.pem
```

Para inserir o certificado auto-assinado, devemos seguir as mesmas etapas acima.

#### Incluindo o certificado via linha de comando
Como a rota é um service, esceutamos o comando para listar os services e identificar o necessário
```shell
oc get service
```

Caso já exista a rota, execute para remover:
```shell
oc get route
oc delete route flask-alpine
```


Vamos 
```shell
oc create route edge --service flask-alpine --ca-cert='cert.pem' --key='key.pem' --cert='cert.pem'
```
