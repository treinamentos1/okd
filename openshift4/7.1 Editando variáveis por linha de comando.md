## Editando variáveis por linha de comando

Vamos editar o YAML do DeploymentConfig criado anteriormente
```
oc edit dc exemplo-variaveis
```

Procure pelas variáveis inseridas e acrescente mais uma:
```yaml
- name: VERSION
  value: v1
```

Após concluir a mudança, perceba que ao listar todos os recursos, temos uma nova revisão e versão de pods. Também há um novo pod com o status Completed pela nova compilação.
```
oc get all
```

Acessando um dos pods com status Running, podemos confirmar a criação da nova variável.
```
oc rsh NOME_DO_POD
env
```
