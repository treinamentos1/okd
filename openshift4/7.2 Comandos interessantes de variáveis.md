## Comandos interessantes de variáveis

Para ver variáveis de um pod:
```
oc set env pod/NOME_DO_POD
```

Para ver variáveis de um DeploymentoConfig:
```
oc set env dc/NOME_DO_POD
```

Para inserir uma variável no DeploymentConfig
OBS: a sintaxe `--overwrite` é opcional
```
oc set env dc/exemplo-variaveis RESPONSAVEL=Udemy
```

Listando as variáveis do DeploymentConfig
```
oc set env dc/exemplo-variaveis --list
```

Para remover uma variável, basta adicionar o sinal de menos ao final do nome da varíavel.
```
oc set env dc/exemplo-variaveis RESPONSAVEL-
```
