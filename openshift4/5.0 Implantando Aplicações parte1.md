## Implantando Aplicações

S2I = source to image
ImageStream = objeto apontando para a imagem que foi baixada e sendo utilizada pela aplicação.
 - Imagem no repositório
DeploymentConfig = 
 - ReplicationController = Encarregar de aplicar as replicas que precisamos
 - Pods = 
Service = Permite que a aplicação seja acessada dentro do Cluster.
 - Route = Rota para acessar a aplicação.


Executando apenas **new-app** gera um Deployment, neste caso vamos utilizar a criação de um DeploymentConfig.
```
oc new-app --as-deployment-config apasoft/blog
```

No cimando acima, criamos um DeploymentConfig:
- imagestream
- deploymentconfig
- service

### Para ver todos os objetos
```
oc get all -l app=blog
```

### Para ver o ImageStream:
```
oc get is
oc describe is blog
```

### para ver o DeploymentConfig
```
oc get dc
oc describe dc blog
```

### para ver o ReplicationController
```
oc get rc
oc describe rc blog
```

### Para ver os pods
```
oc get pod
```

Neste momento, vemos dois pods, um parado e outro executando. Mas como há dois pods se eu pedi apenas um?
A explicação vêm de que o primeiro pod criado `blog-1-deploy` com o status `Completed` é onde a imagem original será alterada gerando um novo pod da nossa aplicação.

### para ver o Service
```
oc get svc
```
