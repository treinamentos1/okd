# Escalonamento Automatico Autoscaling

Suba um container do Apache ou Nginx para teste.

#### Adicionando limites de CPU e RAM

Em **Applications** -> **Deployments**, selecione o deploy desejado.

Agora vamos clicar em **Actions** e selecionar **Edit Resource Limits**

1 núcleo são 1000 milicores, neste caso, adicionaremos
- CPU
  - **Requests** 100 milicores
  - **Limit** 200 milicores

- Memoria
  - **Requests** 100 milicores
  - **Limit** 200 milicores
Clicaremos em Save.

Isto quer dizer que, o mínimo que precisa de CPU é 10% e o máximo é de 20% de um núcleo.

#### Adicionando auto scalling

Vamos clicar em **Actions** e selecionar **Edit Auto scaler**

- Em **Min Pods**, adicione 1
- Em **Max Pods**, adicione 5
- Em **CPU Request Target** adicione 50

Isto quer dizer que o mínimo de Pods ativos será 1, chegará até 5 Pods. Como adicionamos 50, quer dizer que quando chegar a 50% do limite de recursos do núcleo, será escalonado.

#### Teste de estresse

Vamos utilizar o Apache Benchmark para isto,
no Centos, execute para instalar:
```shell
yum install httpd-tool
```

Após instalar, vamos abrir outra aba do terminal no node master e executamos a seguinte linha para ver o processo de estresse acontecer:
```shell
watch oc get hpa
```

Agora na outra aba do terminal do Master, vamos executar o teste de estresse.
```shell
ab -c 200 -n100000 http://172.30.69.180:8080/
```

Você pode tanto ver na dashboard -> Overview -> Nome do Serviço, como também na aba do terminal aberto. 
